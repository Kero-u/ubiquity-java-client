package com.gitlab.blockdaemon.ubiquity.ws;

import static com.gitlab.blockdaemon.ubiquity.TestUtil.readJsonFile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.hamcrest.core.IsNull;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.gitlab.blockdaemon.ubiquity.UbiquityClient;
import com.gitlab.blockdaemon.ubiquity.UbiquityClientBuilder;
import com.gitlab.blockdaemon.ubiquity.model.Network;
import com.gitlab.blockdaemon.ubiquity.model.Platform;
import com.gitlab.blockdaemon.ubiquity.ws.Notification.BlockIdentifierNotification;
import com.gitlab.blockdaemon.ubiquity.ws.Notification.TxNotification;

import okhttp3.WebSocket;
import okhttp3.WebSocketListener;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

public class WebsocketClientTest {

	private  UbiquityClient client;
	private  MockWebServer mockBackEnd;
	private  String URL;
	private  WebSocketRecorder serverListener;

	@Before
	public  void setUp() throws IOException {
		this.serverListener = new WebSocketRecorder();
		this.mockBackEnd = new MockWebServer();
		this.mockBackEnd.start();
		final MockResponse mockResponse = new MockResponse().withWebSocketUpgrade(this.serverListener);
		this.mockBackEnd.enqueue(mockResponse);
		this.URL = String.format("http://%s:%s", this.mockBackEnd.getHostName(), this.mockBackEnd.getPort());
		this.client = new UbiquityClientBuilder().api(this.URL).build();
	}

	@After
	public void teardown() throws IOException {
		this.serverListener.clear();
	}

	@Test
	public void shouldActivateSubscriptions() throws InterruptedException, ExecutionException, TimeoutException {

		// Given there is an open websocket
		final WebsocketClient wsClient = this.client.ws(Platform.ETHEREUM, Network.MAIN_NET);
		this.serverListener.assertOpen();

		final Subscription<BlockIdentifierNotification> subscription = Subscription.blockIdentifiers((ws, notification) -> assertThat(notification.getSubID(), equalTo(123)));

		// When a subscriber is added
		final Future<Void> answered = wsClient.subscribe(subscription);

		// Then the server should receive the request
		this.serverListener.setNextEventDelegate(new WebSocketListener() {
			@Override
			public void onMessage(WebSocket webSocket, String text) {
				assertThat(text, equalTo("{\"id\":2,\"method\":\"ubiquity.subscribe\",\"params\":{\"channel\":\"ubiquity.block_identifiers\",\"detail\":{}}}"));
				webSocket.send("{ \"id\": 2, \"result\": {\"subID\": 123 }}");
			}
		});

		// And the client should handle the servers response to the subscription request
		answered.get(30, TimeUnit.SECONDS);
		assertThat(wsClient.getActiveSubscriptions().size(), equalTo(1));
		assertThat(wsClient.getActiveSubscriptions().get(123), IsNull.notNullValue());
	}


	@Test
	public void shouldProcessBlockIdentMessageFromSubscription() throws InterruptedException, ExecutionException, TimeoutException, IOException {
		// Given there is an open websocket
		final WebsocketClient wsClient = this.client.ws(Platform.ETHEREUM, Network.MAIN_NET);
		final WebSocket	server = this.serverListener.assertOpen();

		// And a subscription is active
		final BlockingQueue<Notification> recievedMessage = new LinkedBlockingQueue<>();
		final Subscription<BlockIdentifierNotification> subscription = Subscription.blockIdentifiers((ws, notification) -> recievedMessage.add(notification));

		// And the server should receive the request
		this.serverListener.setNextEventDelegate(new WebSocketListener() {
			@Override
			public void onMessage(WebSocket webSocket, String text) {
				webSocket.send("{ \"id\": 2, \"result\": {\"subID\": 123 }}");
			}
		});
		final Future<Void> answered = wsClient.subscribe(subscription);

		// And the client should handle the servers response to the subscription request
		answered.get(30, TimeUnit.SECONDS);

		// When the server sends messages to the client
		server.send(readJsonFile("ws_block_ident.json"));

		// Then the client will pass the message to the correct handler
		final Notification notification = recievedMessage.poll(30, TimeUnit.SECONDS);
		assertThat(notification.getSubID(), equalTo(123));
	}

	@Test
	public void shouldProcessTxMessageFromSubscription() throws InterruptedException, ExecutionException, TimeoutException, IOException {
		// Given there is an open websocket
		final WebsocketClient wsClient = this.client.ws(Platform.ETHEREUM, Network.MAIN_NET);
		final WebSocket	server = this.serverListener.assertOpen();

		// And a subscription is active
		final BlockingQueue<Notification> recievedMessage = new LinkedBlockingQueue<>();
		final Subscription<TxNotification> subscription = Subscription.tx((ws, notification) -> recievedMessage.add(notification));

		final Future<Void> answered = wsClient.subscribe(subscription);
		// And the server should receive the request
		this.serverListener.setNextEventDelegate(new WebSocketListener() {
			@Override
			public void onMessage(WebSocket webSocket, String text) {
				webSocket.send("{ \"id\": 2, \"result\": {\"subID\": 123 }}");
			}
		});

		// And the client should handle the servers response to the subscription request
		answered.get(30, TimeUnit.SECONDS);
		// When the server sends messages to the client
		server.send(readJsonFile("ws_tx.json"));

		// Then the client will pass the message to the correct handler
		final Notification notification = recievedMessage.poll(30, TimeUnit.SECONDS);
		assertThat(notification.getSubID(), equalTo(123));
	}

	@Test
	public void shouldReconnectServerGoingAway() throws InterruptedException, ExecutionException, TimeoutException, IOException {
		// Given there is an open websocket
		final WebsocketClient wsClient = this.client.ws(Platform.ETHEREUM, Network.MAIN_NET).reconnectDelay(100L);
		final WebSocket	server = this.serverListener.assertOpen();

		final Subscription<BlockIdentifierNotification> subscription = Subscription.blockIdentifiers((ws, notification) -> {});

		// And a subscriber is added
		final Future<Void> answered = wsClient.subscribe(subscription);
		this.serverListener.setNextEventDelegate(new WebSocketListener() {
			@Override
			public void onMessage(WebSocket webSocket, String text) {
				assertThat(text, equalTo("{\"id\":2,\"method\":\"ubiquity.subscribe\",\"params\":{\"channel\":\"ubiquity.block_identifiers\",\"detail\":{}}}"));
				webSocket.send("{ \"id\": 2, \"result\": {\"subID\": 123 }}");
			}
		});
		answered.get(30, TimeUnit.SECONDS);

		// When the connection fails
		this.mockBackEnd.enqueue(new MockResponse().withWebSocketUpgrade(this.serverListener));
		server.close(1001, "going away");
		Thread.sleep(3000L);

		// Then the connection should be reopened
		this.serverListener.assertClosing(1000, "");
		this.serverListener.assertClosed(1000, "");

		// And the connection should be reopened subscriptions added again
		this.serverListener.assertOpen();
		this.serverListener.assertTextMessage("{\"id\":3,\"method\":\"ubiquity.subscribe\",\"params\":{\"channel\":\"ubiquity.block_identifiers\",\"detail\":{}}}");
	}

	@Test
	public void shouldReconnectServerNetworkFailure() throws InterruptedException, ExecutionException, TimeoutException, IOException {
		// Given there is an open websocket
		final WebsocketClient wsClient = this.client.ws(Platform.ETHEREUM, Network.MAIN_NET).reconnectDelay(100L);
		this.serverListener.assertOpen();

		final Subscription<BlockIdentifierNotification> subscription = Subscription.blockIdentifiers((ws, notification) -> {});

		// When the connection fails (Non-gracefully closes with open set to true)
		wsClient.cancel();
		wsClient.getOpen().set(true);
		this.serverListener.assertFailure(java.io.EOFException.class);

		// And a failure occurs in the websocket
		wsClient.subscribe(subscription);

		// Then the connection should be reopened subscriptions added again
		final MockResponse mockResponse = new MockResponse().withWebSocketUpgrade(this.serverListener);
		this.mockBackEnd.enqueue(mockResponse);

		this.serverListener.assertOpen();
		this.serverListener.assertTextMessage("{\"id\":3,\"method\":\"ubiquity.subscribe\",\"params\":{\"channel\":\"ubiquity.block_identifiers\",\"detail\":{}}}");
	}

}
