package com.gitlab.blockdaemon.ubiquity.ws;

import java.util.List;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class WebsocketResponse {

	private int id;
	private String method;
	private Params params;
	private Result result;

	@Data
	@NoArgsConstructor
	public static class Params{
		private List<? extends Notification> items;
	}

	@Data
	@NoArgsConstructor
	public static class Result{
		private boolean removed;
		private int subID;
	}
}
