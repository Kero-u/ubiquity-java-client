package com.gitlab.blockdaemon.ubiquity.tx.btc;

import java.math.BigInteger;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=true)
public class Bip38PrivateKeyInfo extends PrivateKeyInfo {
	private final String confirmationCode;
	private final String password;

	public Bip38PrivateKeyInfo(String privateKeyEncoded, String confirmationCode, boolean isPublicKeyCompressed) {
		super(false, PrivateKeyInfo.Type.TYPE_BIP38, privateKeyEncoded, null, isPublicKeyCompressed);
		this.confirmationCode = confirmationCode;
		this.password = null;
	}

	public Bip38PrivateKeyInfo(String privateKeyEncoded, BigInteger privateKeyDecoded, String password, boolean isPublicKeyCompressed) {
		super(false, PrivateKeyInfo.Type.TYPE_BIP38, privateKeyEncoded, privateKeyDecoded, isPublicKeyCompressed);
		this.confirmationCode = null;
		this.password = password;
	}


}
