

# Token


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**type** | **String** | Name of token mechanism | 
**id** | **String** | Token identifier | 
**creator** | **String** | Address that created token |  [optional]



