# BlocksApi

All URIs are relative to *https://ubiquity.api.blockdaemon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getBlock**](BlocksApi.md#getBlock) | **GET** /v2/{platform}/{network}/block/{key} | Block By Number/Hash
[**getBlockIdentifier**](BlocksApi.md#getBlockIdentifier) | **GET** /v2/{platform}/{network}/block_identifier/{key} | Block Identifier By Number/Hash



## getBlock

> Block getBlock(platform, network, key)

Block By Number/Hash

Get a block and all its transactions by the block number or hash

### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.BlocksApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        BlocksApi apiInstance = new BlocksApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        String key = "8000000"; // String | Block number or block hash/ID or Special identifier
        try {
            Block result = apiInstance.getBlock(platform, network, key);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling BlocksApi#getBlock");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **key** | **String**| Block number or block hash/ID or Special identifier |

### Return type

[**Block**](Block.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Block |  -  |
| **400** | Invalid Block Number |  -  |
| **401** | Invalid or expired token |  -  |
| **404** | Not Found |  -  |
| **429** | Rate limit exceeded |  -  |


## getBlockIdentifier

> BlockIdentifier getBlockIdentifier(platform, network, key)

Block Identifier By Number/Hash

Get minimal block identifier by block number or hash

### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.BlocksApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        BlocksApi apiInstance = new BlocksApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        String key = "8000000"; // String | Block number or block hash/ID or Special identifier
        try {
            BlockIdentifier result = apiInstance.getBlockIdentifier(platform, network, key);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling BlocksApi#getBlockIdentifier");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |
 **key** | **String**| Block number or block hash/ID or Special identifier |

### Return type

[**BlockIdentifier**](BlockIdentifier.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json, application/problem+json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Block |  -  |
| **400** | Invalid Block Number |  -  |
| **401** | Invalid or expired token |  -  |
| **404** | Not Found |  -  |
| **429** | Rate limit exceeded |  -  |

