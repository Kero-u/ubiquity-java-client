

# Currency

## oneOf schemas
* [NativeCurrency](NativeCurrency.md)
* [SmartTokenCurrency](SmartTokenCurrency.md)
* [TokenCurrency](TokenCurrency.md)

## Example
```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.model.Currency;
import com.gitlab.blockdaemon.ubiquity.model.NativeCurrency;
import com.gitlab.blockdaemon.ubiquity.model.SmartTokenCurrency;
import com.gitlab.blockdaemon.ubiquity.model.TokenCurrency;

public class Example {
    public static void main(String[] args) {
        Currency exampleCurrency = new Currency();

        // create a new NativeCurrency
        NativeCurrency exampleNativeCurrency = new NativeCurrency();
        // set Currency to NativeCurrency
        exampleCurrency.setActualInstance(exampleNativeCurrency);
        // to get back the NativeCurrency set earlier
        NativeCurrency testNativeCurrency = (NativeCurrency) exampleCurrency.getActualInstance();

        // create a new SmartTokenCurrency
        SmartTokenCurrency exampleSmartTokenCurrency = new SmartTokenCurrency();
        // set Currency to SmartTokenCurrency
        exampleCurrency.setActualInstance(exampleSmartTokenCurrency);
        // to get back the SmartTokenCurrency set earlier
        SmartTokenCurrency testSmartTokenCurrency = (SmartTokenCurrency) exampleCurrency.getActualInstance();

        // create a new TokenCurrency
        TokenCurrency exampleTokenCurrency = new TokenCurrency();
        // set Currency to TokenCurrency
        exampleCurrency.setActualInstance(exampleTokenCurrency);
        // to get back the TokenCurrency set earlier
        TokenCurrency testTokenCurrency = (TokenCurrency) exampleCurrency.getActualInstance();
    }
}
```


