

# TxPageV1


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**total** | **Integer** | Number of items in txs |  [optional]
**items** | [**List&lt;TxV1&gt;**](TxV1.md) |  |  [optional]
**continuation** | **String** | Token to get the next page |  [optional]



