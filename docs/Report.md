

# Report


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields** | [**List&lt;ReportField&gt;**](ReportField.md) | Transaction items | 
**items** | **Integer** | The number of transactions in the report | 
**limit** | **Integer** | The limit number provided in the request or the default |  [optional]
**continuation** | **String** | Continuation token to send in the next request if there are more items |  [optional]



