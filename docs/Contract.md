

# Contract


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**symbol** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**imageUrl** | **String** |  |  [optional]
**type** | **String** |  |  [optional]



