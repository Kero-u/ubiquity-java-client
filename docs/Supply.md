

# Supply


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**maximum** | **String** | Maximum supply |  [optional]
**total** | **String** | Total supply at block height, excluding burnt coins |  [optional]
**totalCreated** | **String** | Total coins created historically up until this block |  [optional]
**totalBurnt** | **String** | Total coins burnt historically up until this block |  [optional]
**created** | **String** | Coins created at this block |  [optional]



