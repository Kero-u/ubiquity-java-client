

# TxDestination

A list of recipients

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**destination** | **String** |  | 
**amount** | **String** | The amount you wish to transfer | 



