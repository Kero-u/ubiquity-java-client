

# Transfer

Transfer of currency from one account to another

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**from** | **String** | Sender address | 
**to** | **String** | Receiver address | 
**currency** | [**Currency**](Currency.md) |  | 
**value** | **String** | Integer string in smallest unit (Satoshis) | 
**fee** | [**Fee**](Fee.md) |  |  [optional]



