# PlatformsApi

All URIs are relative to *https://ubiquity.api.blockdaemon.com*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getPlatform**](PlatformsApi.md#getPlatform) | **GET** /v2/{platform}/{network} | Platform Info
[**getPlatformEndpoints**](PlatformsApi.md#getPlatformEndpoints) | **GET** /v1/{platform}/{network}/ | Platform Info
[**getPlatforms**](PlatformsApi.md#getPlatforms) | **GET** /v2/ | Platforms overview
[**getPlatformsList**](PlatformsApi.md#getPlatformsList) | **GET** /v1/ | Platforms overview



## getPlatform

> PlatformDetail getPlatform(platform, network)

Platform Info

Provides information about supported endpoints and generic platform information.


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.PlatformsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        PlatformsApi apiInstance = new PlatformsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        try {
            PlatformDetail result = apiInstance.getPlatform(platform, network);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling PlatformsApi#getPlatform");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |

### Return type

[**PlatformDetail**](PlatformDetail.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Platform overview |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |


## getPlatformEndpoints

> PlatformDetail getPlatformEndpoints(platform, network)

Platform Info

Provides information about supported endpoints and generic platform information.


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.PlatformsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        PlatformsApi apiInstance = new PlatformsApi(defaultClient);
        String platform = "bitcoin"; // String | Coin platform handle
        String network = "mainnet"; // String | Which network to target. Available networks can be found with /{platform}
        try {
            PlatformDetail result = apiInstance.getPlatformEndpoints(platform, network);
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling PlatformsApi#getPlatformEndpoints");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **platform** | **String**| Coin platform handle |
 **network** | **String**| Which network to target. Available networks can be found with /{platform} |

### Return type

[**PlatformDetail**](PlatformDetail.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Platform overview |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |


## getPlatforms

> PlatformsOverview getPlatforms()

Platforms overview

Provides a list of supported platforms and networks.


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.PlatformsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        PlatformsApi apiInstance = new PlatformsApi(defaultClient);
        try {
            PlatformsOverview result = apiInstance.getPlatforms();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling PlatformsApi#getPlatforms");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**PlatformsOverview**](PlatformsOverview.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Platforms overview |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |


## getPlatformsList

> PlatformsOverview getPlatformsList()

Platforms overview

Provides a list of supported platforms and networks.


### Example

```java
// Import classes:
import com.gitlab.blockdaemon.ubiquity.invoker.ApiClient;
import com.gitlab.blockdaemon.ubiquity.invoker.ApiException;
import com.gitlab.blockdaemon.ubiquity.invoker.Configuration;
import com.gitlab.blockdaemon.ubiquity.invoker.auth.*;
import com.gitlab.blockdaemon.ubiquity.invoker.model.*;
import com.gitlab.blockdaemon.ubiquity.api.PlatformsApi;

public class Example {
    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        defaultClient.setBasePath("https://ubiquity.api.blockdaemon.com");
        
        // Configure HTTP bearer authorization: bearerAuth
        HttpBearerAuth bearerAuth = (HttpBearerAuth) defaultClient.getAuthentication("bearerAuth");
        bearerAuth.setBearerToken("BEARER TOKEN");

        PlatformsApi apiInstance = new PlatformsApi(defaultClient);
        try {
            PlatformsOverview result = apiInstance.getPlatformsList();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling PlatformsApi#getPlatformsList");
            System.err.println("Status code: " + e.getCode());
            System.err.println("Reason: " + e.getResponseBody());
            System.err.println("Response headers: " + e.getResponseHeaders());
            e.printStackTrace();
        }
    }
}
```

### Parameters

This endpoint does not need any parameter.

### Return type

[**PlatformsOverview**](PlatformsOverview.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

### HTTP response details
| Status code | Description | Response headers |
|-------------|-------------|------------------|
| **200** | Platforms overview |  -  |
| **401** | Invalid or expired token |  -  |
| **429** | Rate limit exceeded |  -  |

