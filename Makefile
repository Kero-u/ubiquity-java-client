
.PHONY: generate
generate:
	java -jar openapi-generator-cli-6.1.0.jar generate -i spec/openapi-v1.yaml -c open-api-conf.yaml -g java -o generated \
		--schema-mappings bigint=java.math.BigInteger --type-mappings integer+bigint=bigint
